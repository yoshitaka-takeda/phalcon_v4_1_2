<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'Phalcon_v4_1_2\Models' => APP_PATH . '/common/models/',
    'Phalcon_v4_1_2'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'Phalcon_v4_1_2\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'Phalcon_v4_1_2\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
